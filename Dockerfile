FROM registry.gitlab.com/fdroid/ci-images-client:latest
MAINTAINER hans@guardianproject.info

# These packages are used by the SDK emulator to gather info about the
# system.
run apt-get update && apt-get install -y --no-install-recommends \
                apksigner \
                autoconf \
                automake \
                autopoint \
                build-essential \
                fdroidserver \
                file \
                gnupg \
                libtool \
                m4 \
                openssh-client \
                python3-qrcode \
	&& rm -rf /var/lib/apt/lists/* \
	&& apt-get clean

ENV ANDROID_NDK_VERSION r15c
ENV ANDROID_NDK_PACKAGE android-ndk-${ANDROID_NDK_VERSION}-linux-x86_64.zip
ENV ANDROID_NDK_URL https://dl.google.com/android/repository/${ANDROID_NDK_PACKAGE}
ENV ANDROID_NDK_HOME ${ANDROID_HOME}/android-ndk-${ANDROID_NDK_VERSION}
ENV PATH ${ANDROID_NDK_HOME}:$PATH

RUN wget --quiet --continue "${ANDROID_NDK_URL}"  \
      && echo "0bf02d4e8b85fd770fd7b9b2cdec57f9441f27a2  ${ANDROID_NDK_PACKAGE}" > ${ANDROID_NDK_PACKAGE}.sha1 \
      && sha1sum -c ${ANDROID_NDK_PACKAGE}.sha1 \
      && unzip -qq ${ANDROID_NDK_PACKAGE} -d ${ANDROID_HOME}  \
      && rm -rf ${ANDROID_NDK_PACKAGE}* \
      && chmod -R u+rX,a-w ${ANDROID_NDK_HOME}

COPY test /

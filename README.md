# Orbot Android NDK CI image

This Docker image is used in
[Orbot](https://oniongit.eu/tor-mobile/orbot)'s continuous integration
via Gitlab.  It is built on top of F-Droid's
[ci-images-client](https://gitlab.com/fdroid/ci-images-client) Docker
image.  It includes the Android NDK, and stuff that only the client
tests need, like emulator images.
